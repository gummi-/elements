using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManagement : MonoBehaviour
{

    List<GameObject> Spaces = new List<GameObject>();
    [SerializeField] List<GameObject> Elements = new List<GameObject>();
    [SerializeField] List<GameObject> States = new List<GameObject>();

    GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        SaveSpaces();
    }


    int randomNumber()
    {
        int newRandom = Random.Range(0, Elements.Count);
        return newRandom;
    }

    void SaveSpaces()
    {
        foreach (Transform space in transform)
        {
            Spaces.Add(space.gameObject);
            Instantiate(Elements[randomNumber()], space.transform.position, Quaternion.identity, space.transform);
        }
    }
}
