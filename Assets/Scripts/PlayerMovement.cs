using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    private CharacterController controller;
    private Vector3 direction;
    public float forwardSpeed;
   
    private int desiredLane = 1; //0:left, 1:middle, 2:right
    public float laneDistance = 3; //The distance between tow lanes
   
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }


    // Update is called once per frame
    void Update()
    {
        direction.x = forwardSpeed;

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            desiredLane++;
            if (desiredLane == 3)
            {
                desiredLane = 2;
            }
           
        }
       
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            desiredLane--;
            if (desiredLane == -1)
            {
                desiredLane = 0;
            }
           
        }

        Vector3 targetPosition = transform.position.x * transform.right + transform.position.y * transform.up;

   
        if (desiredLane == 0)
        {
            targetPosition += Vector3.forward * laneDistance;
        }else if (desiredLane == 2)
        {
            targetPosition += Vector3.back * laneDistance;
        }

        transform.position = targetPosition;
    }
   
    void FixedUpdate()
    {
        controller.Move(direction * Time.fixedDeltaTime);
    }

   
 
   
}