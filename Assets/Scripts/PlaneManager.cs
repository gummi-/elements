﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneManager : MonoBehaviour
{

    GameObject lastPlane;
    [SerializeField] GameObject planePrefab;
    [SerializeField] GameObject lastPlanePrefab;
    GameObject newPlane;
    float parentScaleX;


    // Start is called before the first frame update
    void Start()
    {
        lastPlane = Instantiate(planePrefab);
        parentScaleX = planePrefab.transform.localScale.x;
    }

    private void Update()
    {
        CheckEndCount();
    }

    public void CreatePlane(GameObject currentPlane)
    {
        if(Scores.endCount <= 2)
        {
            newPlane = Instantiate(planePrefab);
            newPlane.transform.position = new Vector3(currentPlane.transform.parent.transform.position.x + parentScaleX, 0, 0);
        }
        else
        {
            newPlane = Instantiate(lastPlanePrefab);
            newPlane.transform.position = new Vector3(currentPlane.transform.parent.transform.position.x + parentScaleX, 0, 0);
        }
        
    }

    void CheckEndCount()
    {
        if(Scores.endCount >= 2 && newPlane != null)
        {
            newPlane.transform.Find("GameOver").gameObject.SetActive(true);
            newPlane.transform.Find("End").gameObject.SetActive(false);
        }
    }

}


