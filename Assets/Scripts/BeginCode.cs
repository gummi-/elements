using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginCode : MonoBehaviour
{
    [SerializeField] GameObject gameController;

    bool created = false;

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && created == false)
        {
            gameController.GetComponent<PlaneManager>().CreatePlane(gameObject);
            created = true;
        }
    }
}

