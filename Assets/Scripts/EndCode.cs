using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCode : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            Invoke("DestroySelf", 1f);
            Scores.endCount++;
        }
    }

    void DestroySelf()
    {
        Destroy(transform.parent.gameObject);
    }
}

